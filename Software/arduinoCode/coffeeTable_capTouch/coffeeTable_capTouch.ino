/*********************************************************
This is a library for the MPR121 12-channel Capacitive touch sensor

Designed specifically to work with the MPR121 Breakout in the Adafruit shop 
  ----> https://www.adafruit.com/products/

These sensors use I2C communicate, at least 2 pins are required 
to interface

Adafruit invests time and resources providing this open source code, 
please support Adafruit and open-source hardware by purchasing 
products from Adafruit!

Written by Limor Fried/Ladyada for Adafruit Industries.  
BSD license, all text above must be included in any redistribution
**********************************************************/

#include <Wire.h>
#include "Adafruit_MPR121.h"


// You can have up to 4 on one i2c bus but one is enough for testing!
Adafruit_MPR121 cap1 = Adafruit_MPR121();
Adafruit_MPR121 cap2 = Adafruit_MPR121();
Adafruit_MPR121 cap3 = Adafruit_MPR121();
Adafruit_MPR121 cap4 = Adafruit_MPR121();

// Arrays
int touched1[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
int touched2[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
int touched3[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
int touched4[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };



void setup() {
  while (!Serial);        // needed to keep leonardo/micro from starting too fast!

  Serial.begin(9600);
  Serial.println("Adafruit MPR121 Capacitive Touch sensor test"); 
  
  // Default address is 0x5A, if tied to 3.3V its 0x5B
  // If tied to SDA its 0x5C and if SCL then 0x5D
  if (!cap1.begin(0x5A)) {
    Serial.println("MPR121 not found, check wiring?");
    while (1);
  }
  Serial.println("cap1 found!");


  if (!cap2.begin(0x5B)) {
    Serial.println("MPR121 not found, check wiring?");
    while (1);
  }
  Serial.println("cap2 found!");


  if (!cap3.begin(0x5C)) {
    Serial.println("MPR121 not found, check wiring?");
    while (1);
  }
  Serial.println("cap3 found!");


  if (!cap4.begin(0x5D)) {
    Serial.println("MPR121 not found, check wiring?");
    while (1);
  }
  Serial.println("cap4 found!");

}



void loop() {
  // Get the currently touched pads
  currtouched1 = cap1.touched();
  currtouched2 = cap2.touched();
  currtouched3 = cap3.touched();
  currtouched4 = cap4.touched();

  cap1.setThresholds(16,8);
  cap2.setThresholds(16,8);
  cap3.setThresholds(16,8);
  cap4.setThresholds(16,8);

  for (uint8_t i=0; i<12; i++) {
    touched1[i] = (currtouched1 & _BV(i));
    if (i == 11) {
      Serial.write("Left1");
      Serial.write(touched[]); 
    }
  }

  for (uint8_t j=0; j<12; j++) {
    touched2[j] = (currtouched2 & _BV(j));
    if (j == 11) {
      Serial.write("Left2");
      Serial.write(touched[]); 
    }
  }

  for (uint8_t k=0; k<12; k++) {
    touched3[k] = (currtouched3 & _BV(k));
    if (k == 11) {
      Serial.write("Right1");
      Serial.write(touched[]); 
    }
  }

  for (uint8_t l=0; l<12; l++) {
    touched4[l] = (currtouched4 & _BV(l));
    if (l == 11) {
      Serial.write("Right2");
      Serial.write(touched[]); 
    }
  }

return;

}
