#!/bin/sh

# Driver amps on
gpio export 4 out &
# gpio -g write 4 1 &
echo 1 > /sys/class/gpio/gpio4/value &

sleep 10s

# Launch instrument
/usr/bin/java -jar /home/pi/Wekinator/dist/WekiMini.jar /home/pi/Instruments/musicalchairs/Software/wekiProjects/ottoCushion/ottoCushion.wekproj &
pd -nogui -rt /home/pi/Instruments/musicalchairs/Software/pdPatches/ottoman_GUI.pd &
