#!/bin/sh

# Driver amps on
gpio export 4 out &
# gpio -g write 4 1 &
echo 1 > /sys/class/gpio/gpio4/value &

# Subwoofer amps on
gpio export 17 out &
# gpio -g write 17 1 &
echo 1 > /sys/class/gpio/gpio17/value &

sleep 10s

# Launch instrument
/usr/bin/java -jar /home/pi/Wekinator/dist/WekiMini.jar /home/pi/Instruments/musicalchairs/Software/wekiProjects/seatCushion/seatCushion.wekproj &
/usr/bin/java -jar /home/pi/Wekinator/dist/WekiMini.jar /home/pi/Instruments/musicalchairs/Software/wekiProjects/backCushion/backCushion.wekproj &
pd -nogui -rt /home/pi/Instruments/musicalchairs/Software/pdPatches/wingBack_GUI.pd &
