#!/bin/sh

# Driver amps on
gpio export 4 out &
# gpio -g write 4 1 &
echo 1 > /sys/class/gpio/gpio4/value &

# Subwoofer amps on
gpio export 17 out &
# gpio -g write 17 1 &
echo 1 > /sys/class/gpio/gpio17/value &

sleep 10s

# Launch instrument
pd -nogui -noadc -rt /home/pi/Instruments/musicalchairs/Software/pdPatches/coffeeTable_GUI.pd &
