/*
oscP5/netP5 - https://github.com/sojamo/oscp5/blob/master/LICENSE
processing.io - https://github.com/processing/processing/blob/master/license.txt
simpletouch - https://github.com/gohai/processing-simpletouch/blob/master/src/gohai/simpletouch/SimpleTouch.java
*/


import oscP5.*;
import netP5.*;
import processing.io.*;
import gohai.simpletouch.*;


//Text Parameters
int textH = 25;

//OSC Variable Init
OscP5 oscP5;
NetAddress toPureData;

//Photo Resistor Variable Init
int leftResPin = 17;
int rightResPin = 18;
float leftResVal = 0;
float rightResVal = 0;

//Spectral Variables Init

float spectralW = 72;
float rectW = 18;
float[] spectralY = new float[44];
float aScalar = 100;


//TouchScreen Init
SimpleTouch touchscreen;
int onOff = 0;

void setup() {
  fullScreen();
  colorMode(HSB, 360, 100, 100);
  noStroke();
  noCursor();
  
//OSC setup
  toPureData = new NetAddress("127.0.0.1",12000);
  oscP5 = new OscP5(this,12005);
  
//PhotoResistor setup
  GPIO.pinMode(leftResPin, GPIO.INPUT);
  GPIO.pinMode(rightResPin,GPIO.INPUT);
 
//TouchScreen setup
  println("Available input devices:");
  String[] devs = SimpleTouch.list();
  printArray(devs);

  if (devs.length == 0) {
    println("No input devices available");
    exit();
  }

  touchscreen = new SimpleTouch(this, devs[1]);
  println("Opened device: " + touchscreen.name());
}

void draw() {
  background(250);
  
  textSize(textH);
  fill(50, 150, 150);
  text("Saw", 0, textH);
  
  textSize(textH);
  fill(50, 150, 150);
  text("Osc", 0, (height-textH+1));
  
  textSize(textH);
  String t = "Osc";
  float tW = textWidth(t);
  fill(50, 150, 150);
  text("200Hz", tW+1, (height));
  
  textSize(textH);
  String s = "5kHz";
  float sW = textWidth(s);
  fill(50, 150, 150);
  text("5kHz", (width-sW), (height));
  
  

  SimpleTouchEvt touches[] = touchscreen.touches();
  for (SimpleTouchEvt touch : touches) {
    
    //log resistor values
    leftResVal = GPIO.digitalRead(leftResPin);
    rightResVal = GPIO.digitalRead(rightResPin);
    
    // the id value is used to track each touch
    // we use it to assign a unique color
    fill((touch.id * 100) % 360, 100, 100);
    // x and y are values from 0.0 to 1.0
    ellipse(width * touch.x, height * touch.y, 25, 25);
    
    oscP5.send(new OscMessage("/touchData").add(touch.x).add(touch.y),toPureData);
    oscP5.send(new OscMessage("/resistorData").add(leftResVal).add(rightResVal),toPureData);
  }
  
  if (touches.length == 0) {
   onOff = 0; 
  }
  else {
   onOff = 1; 
  }
  oscP5.send(new OscMessage("/touches").add(onOff),toPureData);
  
  //draw spectral rectangles
  for(int i=0; i<44; i=i+4) {
    rect(i*rectW, height-spectralY[i], rectW, spectralY[i]);
    fill(255, 10, 10, 255);
    }
  for(int i=1; i<44; i=i+4) {
    rect(i*rectW, height-spectralY[i], rectW, spectralY[i]);
    fill(10, 255, 10, 255);
    }
  for(int i=2; i<44; i=i+4) {
    rect(i*rectW, height-spectralY[i], rectW, spectralY[i]);
    fill(10, 10, 255, 255);
    }  
  for(int i=3; i<44; i=i+4) {
    rect(i*rectW, height-spectralY[i], rectW, spectralY[i]);
    fill(0, 0, 0, 255);
    } 
}

void oscEvent(OscMessage theOscMessage) {  
    if (theOscMessage.checkAddrPattern ("/spectralValues") == true) {
      for(int j=0; j<44; j++) {
        spectralY[j] = theOscMessage.get(j+1).floatValue();
      }
    return;  
    }
}  
