#include <Filter.h>

/*
Adapted from the "a_readAllAnalog.ino" example provided by Plusea. 

License:
https://github.com/plusea/CODE/blob/master/libraries/License.txt
*/


byte analogPins[] = { 
  A0, A1, A2, A3, A4, A5 };  

//raw variables
int val1;
int val2;
int val3;
int val4;
int val5;
int val6;

//filter variables
ExponentialFilter<float> filteredVal1(5,0);
ExponentialFilter<float> filteredVal2(5,0);
ExponentialFilter<float> filteredVal3(5,0);
ExponentialFilter<float> filteredVal4(5,0);
ExponentialFilter<float> filteredVal5(5,0);
ExponentialFilter<float> filteredVal6(5,0);

//output variables
float sensor1;
float sensor2;
float sensor3;
float sensor4;
float sensor5;
float sensor6;

void setup() {
  for(int i=0;i<6;i++){
  pinMode(analogPins[i], INPUT_PULLUP);  
  }
  Serial.begin(9600);
}

void loop() {
  val1=analogRead(0);
  val2=analogRead(1);
  val3=analogRead(2);
  val4=analogRead(3);
  val5=analogRead(4);
  val6=analogRead(5);

  filteredVal1.Filter(val1);
  filteredVal2.Filter(val2);
  filteredVal3.Filter(val3);
  filteredVal4.Filter(val4);
  filteredVal5.Filter(val5);
  filteredVal6.Filter(val6);

  float sensor1 = filteredVal1.Current();
  float sensor2 = filteredVal2.Current();
  float sensor3 = filteredVal3.Current();
  float sensor4 = filteredVal4.Current();
  float sensor5 = filteredVal5.Current();
  float sensor6 = filteredVal6.Current();


  
  //Send to Pd
  Serial.write(1);
  Serial.write(sensor1);
  Serial.write(2);
  Serial.write(sensor2);
  Serial.write(3);
  Serial.write(sensor3);
  Serial.write(4);
  Serial.write(sensor4);
  Serial.write(5);
  Serial.write(sensor5);
  Serial.write(6);
  Serial.write(sensor6);


  //delay 10ms to prevent bogging down
  delay(10);
}
