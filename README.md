# Musical Chairs

Perpetual Beta distribution repo for my dissertation project *Musical Chairs*


## For Current Developer Card:
**On desktop or laptop:**
- Click [MusicalChairs](https://drive.google.com/file/d/1zEQosb1YyAztU_JYUs_eiABIyrzTnggm/view?usp=sharing) to download the developer-sdImage currently running on all *Musical Chairs* installation pieces.
This image includes an automatic git-pull on startup to retrieve upcoming code updates. See the project [Wiki page](https://gitlab.com/blessing5150/musicalchairs/wikis/SD-Build) for SD build details if you prefer to roll your own. 
- Unzip MusicalChairs.zip
- Copy MusicalChairs-0.1-1.dd to a 16GB or larger SD-card. Follow [Raspberry Pi's Installation Instructions](https://www.raspberrypi.org/documentation/installation/installing-images/README.md) for this process. 

**On the Pi:**
- Open Terminal and type: sudo raspi-config
- Go to Change User Password to set a custom password (default is tmpPwd123!)
- Go to Network Options -> Hostname to set a custom hostname (default is MusicalChairs-01-1)
- Go to Advanced Options -> Expand Filesystem to resize partitions
- Choose Finish to close raspi-config and reboot.

**On the Pi:**
- Open Terminal and type: sudo nano /home/pi/Startup/musicalChairs_launch.sh
- Uncomment one of the instrument launch scripts to have that code launch on boot.