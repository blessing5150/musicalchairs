#!/bin/sh

# Driver amps on
gpio export 4 out &
# gpio -g write 4 1 &
echo 1 > /sys/class/gpio/gpio4/value &

sleep 10s

# Launch instrument
pd -nogui -noadc -rt /home/pi/Instruments/musicalchairs/Software/pdPatches/endTable_GUI.pd &
processing-java --sketch=/home/pi/Instruments/musicalchairs/Software/processingCode/endTop2Pd --present &
